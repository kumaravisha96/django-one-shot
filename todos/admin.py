from django.contrib import admin
from .models import TodoList
from .models import TodoItem



class TodoListAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'name',
    )

admin.site.register(TodoList)

class TodoItemAdmin(admin.ModelAdmin):
    list_display = (
        'task',
        'due_date',
        'is_completed',
    )

admin.site.register(TodoItem)