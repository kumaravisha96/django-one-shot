from django.shortcuts import render
from .models import TodoList
from .forms import TodoListForm


def todo_list_list(request):
    todos = TodoList.objects.all()
    context = {
    'todos': todos,
}

    return render(request, 'todos/list.html', context)


def todo_list_detail(request, id):
    todo_list = TodoList.objects.get(pk=id)
    tasks = todo_list.tasks.all()
    context = {
        'todo_list': todo_list,
        'tasks': tasks
}
    return render(request, 'todos/todo_list_detail.html', context)


def todo_list_view(request):
    todo_lists = TodoList.objects.all()
    for todo_list in todo_lists:
        todo_list.task_count = todo_list.tasks.count()
    context = {
        'todo_lists': todo_lists
}
    return render(request, 'todos/todo_list.html', context)



def create_todo_list(request):
    if request.method == 'POST':
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()
    return render(request, 'todos/create_todo_list.html', {'form': form})
