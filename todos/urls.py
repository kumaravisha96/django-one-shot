from django.urls import path
from .views import todo_list_list
from . import views
from .views import todo_list_detail


urlpatterns = [
    path('<int:id>/', todo_list_detail, name='todo_list_detail'),
    path('', todo_list_list, name='todo_list_list'),

]
